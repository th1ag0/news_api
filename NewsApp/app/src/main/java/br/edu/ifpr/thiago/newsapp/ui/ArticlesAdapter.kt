package br.edu.ifpr.thiago.newsapi.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.thiago.newsapp.R
import br.edu.ifpr.thiago.newsapp.entities.Article

class ArticlesAdapter(var articles:MutableList<Article>): RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ArticleViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.card_article, parent, false)
    )

    override fun getItemCount()= articles.size

    override fun onBindViewHolder(holder: ArticlesAdapter.ArticleViewHolder, position: Int) {
        holder.fillUI(articles[position])
    }

    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun fillUI(article:Article){

        }
    }
}