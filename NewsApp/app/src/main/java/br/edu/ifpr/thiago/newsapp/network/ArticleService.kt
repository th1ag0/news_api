package br.edu.ifpr.thiago.newsapi.network

import br.edu.ifpr.thiago.newsapp.entities.News
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleService {
    @GET("top-headlines")
    fun getNews(
        @Query("q")
        category: String,
        @Query("country")
        country: String = "br",
        @Query("apiKey")
        apiKey: String = "3fa4f7b40e134980ab1381b15d1a7c73"
    ): Call<News>
}