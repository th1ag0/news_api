package br.edu.ifpr.thiago.newsapp.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.thiago.newsapi.network.ArticleService
import br.edu.ifpr.thiago.newsapi.ui.ArticlesAdapter
import br.edu.ifpr.thiago.newsapp.R
import br.edu.ifpr.thiago.newsapp.entities.Article
import br.edu.ifpr.thiago.newsapp.entities.News
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    lateinit var retrofit: Retrofit
    lateinit var service: ArticleService
    lateinit var adapter: ArticlesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        configureRetrofit()
    }


    fun configureRetrofit() {
        retrofit = Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ArticleService>(ArticleService::class.java)

        service.getNews("","br").enqueue(object : Callback<News> {
            override fun onFailure(call: Call<News>, t: Throwable) {
                Log.e("ERRO", "ERRO", t)
            }

            override fun onResponse(call: Call<News>, response: Response<News>) {
                val tasks = response.body()
                if (tasks != null)
                    loadRecyclerView(tasks)
            }

        })
    }

//    private fun loadRecyclerView(articles: News) {
//        adapter = ArticlesAdapter(articles.toMutableList(), this)
//        listTask.adapter = adapter
//        listTask.layoutManager = LinearLayoutManager(
//            this,
//            RecyclerView.VERTICAL, false
//        )
//
//    }
}
